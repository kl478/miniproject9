# Mini Project 9: Streamlit App with a Hugging Face Model

[Demo Video](https://gitlab.com/kl478/miniproject9/-/blob/main/screenshots/demo.mp4?ref_type=heads)

## Accessing the Deployed App
[https://reby0217-miniproject9.streamlit.app](https://reby0217-miniproject9.streamlit.app).

## Project Description
This project demonstrates the integration of a cutting-edge Language Model from Hugging Face within a Streamlit web application. It aims to showcase the power of open-source language models by enabling users to generate text based on prompts they provide. This app leverages the GPT-2 model for generating diverse, context-aware responses ranging from narrative text to simulated dialogues.

## Goals
- Create a website using Streamlit
- Connect to an open source LLM (Hugging Face)
- Deploy model via Streamlit or other service (accessible via browser)

## Project Setup
1. Ensure Python 3.9+ is installed on your system.
2. Install Streamlit and other required packages:
   ```bash
   pip3 install streamlit transformers tensorflow tf-keras
   ```
3. Verify the installation by running:
    ```bash
    streamlit hello
    ```


### Additional Packages
Create a `requirements.txt` file with the following content to manage dependencies:
```plaintext
streamlit
transformers
tensorflow
tf-keras
```

Install the required packages:
```bash
pip3 install -r requirements.txt
```

### Streamlit Web Application
1. Create a Python script named `LLM.py` for the Streamlit app.
2. Utilize the `transformers` library to integrate the GPT-2 model.
3. Implement a user interface for submitting text prompts and displaying generated text.

### Local Testing
To test the application locally, navigate to the project directory and execute:
```bash
streamlit run LLM.py
```
Visit `http://localhost:8501` in your web browser to interact with the app.

### Deployment on Streamlit Cloud
1. Push your project to a GitHub repository.
2. Sign up or log in to [Streamlit Cloud](https://streamlit.io/).
3. Choose "New app", then select your `repository` and the `branch` where `LLM.py` is located and specify your `main file path` (the main file path for our proejct is LLM.py).
4. You can modify the given APP URL as long as your provided URL's domain is available (Optional).
5. Click "Deploy!" to launch your app. Access your deployed app using the provided URL.


## Deliverables
   ![local](screenshots/local.png)

   ![Streamlit](screenshots/streamlit.png)

   ![deployed](screenshots/deployed.png)
